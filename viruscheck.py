#!/usr/bin/env python2
import pprint
import hashlib
import sys
from virus_total_apis import PublicApi as VirusTotalPublicApi
import re
import os
import os.path
import time
import pickle

verbose = 0
verbose_re = re.compile("^-v+")
for o in sys.argv[1:]:
    if verbose_re.match(o):
        verbose+=len(o)-1
    else:
        sys.stderr.write("Invalid argument: %s\n" % (o))
        sys.exit(1)

mypath = os.path.realpath(__file__ + "/..")

min_positives = 3
sleep_time = 61
max_tries = 3
found_re = re.compile("^(.*)(: .* FOUND)( [0-9]+/[0-9]+)?$")
cache = {}
ignores = set()
try:
    with open(mypath + "/cache.pickle", "rb") as fd:
        cache = pickle.load(fd) or {}
except:
    pass
try:
    with open(mypath + "/ignores.txt", "r") as fd:
        ignores = set(r.strip() for r in fd)
except:
    pass

still_to_check = []

with open(mypath + "/virustotal.key", "rb") as fd:
    API_KEY = fd.read().strip()

vt = VirusTotalPublicApi(API_KEY)

def do_vt_api_call(call, *args, **kwargs):
    for i in range(max_tries):
        report = call(*args, **kwargs)
        if report["response_code"]!=204:
            return report
        time.sleep(sleep_time)
    raise RuntimeError("Keep getting 204 from VirusTotal =(")

def print_row(r, report):
    print r,
    if verbose>0:
        print "%d/%d" % (report["results"]["positives"], len(report["results"]["scans"])),
    if verbose>1:
        print pprint.pformat(report),
    print

for r in sys.stdin:
    r = r.strip()
    m = found_re.match(r)
    if m:
        to_scan = m.group(1)
        r = m.group(1) + m.group(2)
        if to_scan in ignores:
            continue
        with open(to_scan, "rb") as fd:
            sha1 = hashlib.sha1(fd.read()).hexdigest()
        if sha1 in ignores:
            continue
        if os.path.getsize(to_scan)>30*1024*1024:
            print r,
            if verbose>0:
                print "file too big, not checked",
            print
            continue
        if sha1 in cache:
            report = cache[sha1]
        else:
            report = do_vt_api_call(vt.get_file_report, sha1)
        if report["results"]["response_code"]==1:
            cache[sha1]=report
            if report["results"]["positives"] > min_positives:
                print_row(r, report)
        else:
            do_vt_api_call(vt.scan_file, to_scan)
            still_to_check.append((to_scan, sha1, r))
    else:
        print r

for name, sha1, r in still_to_check:
    report = do_vt_api_call(vt.get_file_report, sha1)
    while report["results"]["response_code"]!=1:
        time.sleep(30)
        report = do_vt_api_call(vt.get_file_report, sha1)
    cache[sha1]=report
    if report["results"]["positives"] > min_positives:
        print_row(r, report)

try:
    with open(mypath + "/cache.pickle", "wb") as fd:
        pickle.dump(cache, fd)
except IOError as ex:
    if ex.errno==13:
        if verbose > 1:
            sys.stderr.write("Warning: couldn't save cache.pickle (permission denied)\n")
    else:
        raise
