# virustotal_clam_pipe #

This script was born to mitigate the annoyance of checking the false-positives of each daily ClamAV scan over our file server; the routine was:

- maybe once a week receive the mail message with some new false positives (plus *maybe* a real threat);
- send each file over to [VirusTotal](https://www.virustotal.com/);
- check that it actually *is* a false positive.

Since even the last part is quite algorithmic (the distinction between false positives and real threats usually is 3/50 vs 45/50), I decided that scripting was to come to the rescue.

## Prerequisites ##

- Python 2.6+
- [`virustotal-api`](https://pypi.python.org/pypi/virustotal-api/) module (`pip install virustotal-api`)

## How to use ##

- register to VirusTotal to obtain a free VirusTotal API key;
- write it in the file `virustotal.key`, to be put in the same directory of the script;
- put the script in pipe after `clam_scan` or `clamd_scan`; it will double-check each virus-found row by sending the file to VirusTotal and checking if it is marked as a virus by more than `min_positives` antiviruses; in this case it will let the row pass (adding [positives/total] at the end if the `-v` option is specified), otherwise the line is dropped.

## Ignore list ##

There are files you may know to be safe, regardless of what ClamAV and VirusTotal think; you can add them to `ignores.txt`. You can freely add either their fully qualified path or (better) their SHA1 hash.

Notice that no particular format is enforced in the ignore list, it's just searched for full paths and SHA1 hashes on each line. This means that you can freely add comments as long as they surely cannot be confused with full paths or SHA1 hashes (personally, I like comments starting the line with `#`).

## Cache ##

To avoid wasting time very day with requests that were already made 24 hours earlier (VirustTotal throttles lookups even if they are just by hash, and not by file submit) the script keeps a pickle-d cache in `cache.pickle`.

This means that:

- once you get some results from VirusTotal, they are cached forever; no automatic cache refresh is ever done, if you want it, you have to do it manually (also, you probably have to re-submit the file manually to VirusTotal too, since it too does give cached results unless explicitly requested to re-examine a file today);
- you can see how many false positives you are avoiding by checking the size of the dictionary stored into it;
- you should not run multiple instances of the script concurrently; since the cache is loaded at start and saved on quit, you will end with one of the saves ignored in the best case (and with a corrupted cache in the worst case);
- the script should have the permissions to write to `cache.pickle` (although a write error is not considered fatal).

## Verbose ##

Adding `-v` switches to the command line increase verbosity.

- no `-v`: the lines are left untouched;
- `-v`: at the end of the "virus lines", an annotation in the form `[positive/total]` is appended, reflecting the results from VirusTotal;
- `-vv` or `-v -v`: at the end of the "virus lines" is also printed the full JSON dump of the VirusTotal response; useful if you want to check manually how bad the results are without going to the site.

# License #

Standard 3-clause BSD; see LICENSE.